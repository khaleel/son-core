import Joi from 'joi';

export default {
  // Common

  // GET /api/categories
  // GET /api/orders
  // GET /api/products
  // GET /api/recurringOrders
  // GET /api/roles
  // GET /api/suppliers/staff
  getList: {
    query: {
      skip: Joi.number().integer().min(0).required(),
      limit: Joi.number().integer().min(1).required()
    }
  },

  // Admin APIs

  // GET /api/admins
  adminsList: {
    query: {
      skip: Joi.number().integer().min(0).required(),
      limit: Joi.number().integer().min(1).required(),
      status: Joi.array()
    }
  },
  // PUT /api/admins
  updateCurrentAdmin: {
    body: {
      firstName: Joi.string().min(2).max(20).required(),
      lastName: Joi.string().min(2).max(20).required(),
      email: Joi.string().email().required(),
      password: Joi.string().min(6).max(20),
      mobileNumber: Joi.string().regex(/^9665[0-9]{8}$/).required()
    }
  },
  // POST /api/admins
  createAdmin: {
    body: {
      firstName: Joi.string().min(2).max(20).required(),
      lastName: Joi.string().min(2).max(20).required(),
      email: Joi.string().email().required(),
      password: Joi.string().min(6).max(20).required(),
      mobileNumber: Joi.string().regex(/^9665[0-9]{8}$/).required(),
      role: Joi.string().hex().required(),
      language: Joi.string().required()
    }
  },
  // PUT /api/admins/:adminId
  updateAdmin: {
    body: {
      firstName: Joi.string().min(2).max(20).required(),
      lastName: Joi.string().min(2).max(20).required(),
      email: Joi.string().email().required(),
      password: Joi.string().min(6).max(20),
      mobileNumber: Joi.string().regex(/^9665[0-9]{8}$/).required(),
      status: Joi.string().allow('Active', 'Suspended', 'Blocked').required(),
      role: Joi.string().hex().required()
    },
    params: {
      adminId: Joi.string().hex().required()
    }
  },
  // GET /api/admins/:adminId
  getAdmin: {
    params: {
      adminId: Joi.string().hex().required()
    }
  },
  // GET /api/admins/reports/orders
  // GET /api/admins/reports/transactions
  adminGetOrdersReport: {
    query: {
      skip: Joi.number().integer().min(0).required(),
      limit: Joi.number().integer().min(1).required(),
      startDate: Joi.string().required(),
      endDate: Joi.string().required(),
      status: Joi.array()
    }
  },

  // Auth APIs

  // POST /api/auth/login
  login: {
    body: {
      username: Joi.string().required(),
      password: Joi.string().min(6).max(20).required()
    }
  },
  // PUT /api/auth/changePassword
  userChangePassword: {
    body: {
      oldPassword: Joi.string().min(6).max(20).required(),
      newPassword: Joi.string().min(6).max(20).required()
    }
  },
  // PUT /api/auth/resetPassword
  userResetPassword: {
    body: {
      userId: Joi.string().hex().required(),
      newPassword: Joi.string().min(6).max(20).required()
    }
  },

  // Role APIs

  // GET /api/roles
  roleList: {
    query: {
      skip: Joi.number().integer().min(0),
      limit: Joi.number().integer().min(1)
    }
  },
  // PUT /api/roles
  removeRole: {
    body: {
      alternativeRoleId: Joi.string().hex().required()
    }
  },

  // Cart APIs

  // POST /api/carts
  addToCart: {
    body: {
      product: Joi.string().hex().required(),
      quantity: Joi.number().integer().min(1).required()
    }
  },
  // PUT /api/carts/product/:productId
  updateCartProductQuantity: {
    params: {
      product: Joi.string().hex().required()
    },
    query: {
      quantity: Joi.number().integer().min(1).required()
    }
  },
  // DELETE /api/carts/product/:product
  removeCartProduct: {
    params: {
      product: Joi.string().hex().required()
    }
  },
  // POST /api/carts/:cartId/checkout
  checkoutCart: {
    params: {
      cartId: Joi.string().hex().required()
    },
    body: {
      orderIntervalType: Joi.string(),
      orderFrequency: Joi.number().integer().min(1),
      startDate: Joi.date()
    }
  },
  // GET /api/cart/:cartId
  getByCartId: {
    params: {
      cartId: Joi.string().hex()
    }
  },
  getBySupplierId: {
    params: {
      supplierId: Joi.string().hex()
    }
  },
  // DELETE /api/cart/:cartId
  resetCart: {
    params: {
      cartId: Joi.string().hex().required()
    }
  },

  // Category APIs

  // POST api/categories
  createCategory: {
    body: {
      arabicName: Joi.string().required(),
      englishName: Joi.string().required(),
      status: Joi.string().allow('Active', 'Hidden'),
      parentCategory: Joi.string().hex()
    }
  },
  // GET /api/categories/:categoryId
  getCategory: {
    params: {
      categoryId: Joi.string().hex().required()
    }
  },
  // PUT /api/categories/:categoryId
  updateCategory: {
    params: {
      categoryId: Joi.string().hex().required()
    },
    body: {
      arabicName: Joi.string().required(),
      englishName: Joi.string().required(),
      status: Joi.string().allow('Active', 'Hidden').required(),
      parentCategory: Joi.string().hex()
    }
  },
  // DELETE /api/categories/:categoryId
  deleteCategory: {
    params: {
      categoryId: Joi.string().hex().required()
    }
  },
  // GET /api/categories/products/:categoryId
  getCategoryProducts: {
    params: {
      categoryId: Joi.string().hex().required()
    }
  },

  // Customer APIs

  // GET /api/customers
  customerList: {
    query: {
      skip: Joi.number().integer().min(0),
      limit: Joi.number().integer().min(1),
      status: Joi.array(),
      payingSoon: Joi.boolean(),
      missedPayment: Joi.boolean()
    }
  },
  // PUT /api/customers
  updateCustomer: {
    body: {
      coordinates: Joi.array().min(1).max(2),
      representativeName: Joi.string().required(),
      representativePhone: Joi.string().regex(/^9665[0-9]{8}$/),
      representativeEmail: Joi.string().email(),
      firstName: Joi.string().min(2).max(20),
      lastName: Joi.string().min(2).max(20),
      password: Joi.string().min(6).max(20)
    }
  },
  // POST /api/customers
  createCustomer: {
    body: {
      // longitude: Joi.number().min(-180).max(180).required(),
      // latitude: Joi.number().min(-90).max(90).required(),
      coordinates: Joi.array().min(1).max(2).required(),
      representativeName: Joi.string().required(),
      representativePhone: Joi.string().regex(/^9665[0-9]{8}$/),
      representativeEmail: Joi.string().email(),
      commercialRegister: Joi.string().regex(/^[0-9]{10}$/).required(),
      commercialRegisterPhoto: Joi.string().required(),
      userEmail: Joi.string().email().required(),
      userMobilePhone: Joi.string().regex(/^9665[0-9]{8}$/).required(),
      userFirstName: Joi.string().min(2).max(20).required(),
      userLastName: Joi.string().min(2).max(20).required(),
      userPassword: Joi.string().min(6).max(20).required(),
      language: Joi.string().required()
    }
  },
  // POST /api/customers/invite
  inviteCustomer: {
    body: {
      customerEmail: Joi.string().email().required()
    }
  },
  // PUT api/customers/:customerId/supplier
  updateSupplierRelation: {
    params: {
      customerId: Joi.string().hex().required()
    },
    body: {
      creditLimit: Joi.number().min(0),
      paymentInterval: Joi.string().required(),
      status: Joi.string().allow('Active', 'Blocked').required(),
      productPrices: Joi.array()
    }
  },
  // GET /api/customers/:customerId
  // PUT /api/customers/block/:customerId
  // PUT /api/customers/unblock/:customerId
  getCustomer: {
    params: {
      customerId: Joi.string().hex().required()
    }
  },
  // POST/api/customers/specialPrices
  createProductPrice: {
    body: {
      customerId: Joi.string().hex().required(),
      productId: Joi.string().hex().required(),
      price: Joi.number().min(0).required()
    }
  },
  // PUT/api/customers/specialPrices/:productId
  updateProductPrice: {
    body: {
      customerId: Joi.string().hex().required(),
      price: Joi.number().min(0).required()
    }
  },

  getProductPriceList: {
    query: {
      supplierId: Joi.string().hex(),
      customerId: Joi.string().hex(),
      skip: Joi.number().integer().min(0),
      limit: Joi.number().integer().min(1)
    }
  },

  // Order APIs
  // PUT /api/orders/:orderId/accept
  acceptOrder: {
    params: {
      orderId: Joi.string().hex().required()
    },
    body: {
      acceptedProducts: Joi.array().min(1).required()
    }
  },
  // PUT /api/orders/:orderId/reject
  // PUT /api/orders/:orderId/cancel
  // PUT /api/orders/:orderId/prepare
  // PUT /api/orders/:orderId/readyForDelivery
  // PUT /api/orders/:orderId/outForDelivery
  // PUT /api/orders/:orderId/delivered
  // GET /api/orders/:orderId
  getOrderOrUpdateOrderStatus: {
    query: {
      skip: Joi.number().integer().min(0).required(),
      limit: Joi.number().integer().min(1).required()
    },
    params: {
      orderId: Joi.string().hex().required()
    }
  },
  // POST /api/orders/:orderId/review
  createReview: {
    params: {
      orderId: Joi.string().hex().required()
    },
    body: {
      overall: Joi.number().integer().min(1).max(5).required(),
      itemCondition: Joi.number().integer().min(1).max(5).required(),
      delivery: Joi.number().integer().min(1).max(5).required()
    }
  },
  // GET /api/orders
  ordersList: {
    query: {
      skip: Joi.number().integer().min(0).required(),
      limit: Joi.number().integer().min(1).required(),
      supplierId: Joi.string().hex(),
      driverId: Joi.string().hex(),
      status: Joi.array()
    }
  },
  // GET /api/orders/history
  getOrderHistory: {
    query: {
      skip: Joi.number().integer().min(0).required(),
      limit: Joi.number().integer().min(1).required(),
      supplierId: Joi.string().hex(),
      orderId: Joi.string()
    }
  },
  // PUT /api/orders/outForDelivery
  orderOutForDelivery: {
    body: {
      driverId: Joi.string().hex().required()
    }
  },
  // PUT /api/orders/rejectProducts
  rejectProducts: {
    body: {
      products: Joi.array().min(1).required()
    }
  },
  // GET /api/orders/log
  cancelOrReject: {
    body: {
      message: Joi.string()
    }
  },

  // Product APIs

  // POST /api/products
  createProduct: {
    body: {
      categories: Joi.array().min(1).required(),
      arabicName: Joi.string().required(),
      englishName: Joi.string().required(),
      arabicDescription: Joi.string().required(),
      englishDescription: Joi.string().required(),
      sku: Joi.string(),
      store: Joi.string(),
      shelf: Joi.string(),
      price: Joi.number().min(0).required(),
      unit: Joi.string().required(),
      status: Joi.string().allow('Active', 'Hidden').required(),
      coverPhoto: Joi.string().hex().required(),
      images: Joi.array().min(1)
    }
  },
  // GET /api/products/supplier/:supplierId
  getSupplierProducts: {
    params: {
      supplier: Joi.string().hex().required()
    },
    query: {
      skip: Joi.number().integer().min(0),
      limit: Joi.number().integer().min(1)
    }
  },
  // GET /api/products/:productId
  // DELETE /api/products/:productId
  getOrDeleteProduct: {
    params: {
      productId: Joi.string().hex().required()
    }
  },
  // PUT /api/products/:productId
  updateProduct: {
    // params: {
    //   supplier: Joi.string().hex().required()
    // },
    body: {
      categories: Joi.array().min(1).required(),
      arabicName: Joi.string().required(),
      englishName: Joi.string().required(),
      arabicDescription: Joi.string().required(),
      englishDescription: Joi.string().required(),
      sku: Joi.string(),
      store: Joi.string(),
      shelf: Joi.string(),
      price: Joi.number().min(0).required(),
      unit: Joi.string().required(),
      status: Joi.string().allow('Active', 'Hidden').required(),
      images: Joi.array().min(1).required()
    }
  },

  // Recurring Order APIs

  // PUT /api/recurringOrders/:recurringOrderId/cancel
  // GET /api/recurringOrders/:recurringOrderId
  getOrCancelRecurringOrder: {
    params: {
      recurringOrderId: Joi.string().hex().required()
    }
  },

  // Supplier APIs

  // GET /api/suppliers
  supplierList: {
    query: {
      skip: Joi.number().integer().min(0),
      limit: Joi.number().integer().min(1),
      // supplierName: Joi.string(),
      status: Joi.array()
    }
  },
  // GET /api/suppliers/staff
  supplierStaffList: {
    query: {
      skip: Joi.number().integer().min(0),
      limit: Joi.number().integer().min(1),
      // supplierName: Joi.string(),
      status: Joi.array()
    }
  },
  // POST /api/suppliers
  createSupplier: {
    body: {
      coordinates: Joi.array().min(1).max(2),
      address: Joi.string(),
      representativeName: Joi.string().required(),
      commercialRegister: Joi.string().regex(/^[0-9]{10}$/).required(),
      commercialRegisterPhoto: Joi.string().required(),
      userEmail: Joi.string().email().required(),
      userMobilePhone: Joi.string().regex(/^9665[0-9]{8}$/).required(),
      userFirstName: Joi.string().required(),
      userLastName: Joi.string().required(),
      userPassword: Joi.string().min(6).max(20).required(),
      language: Joi.string().required()
    }
  },
  // POST /api/suppliers/staff
  createSupplierStaff: {
    body: {
      email: Joi.string().email(),
      mobileNumber: Joi.string().regex(/^9665[0-9]{8}$/).required(),
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      password: Joi.string().min(6).max(20).required(),
      role: Joi.string().hex().required(),
      status: Joi.string().required(),
      language: Joi.string().required()
    }
  },
  // PUT /api/suppliers/staff/:staffId
  updateSupplierStaff: {
    userEmail: Joi.string().email().required(),
    userMobilePhone: Joi.string().regex(/^9665[0-9]{8}$/).required(),
    userFirstName: Joi.string().required(),
    userLastName: Joi.string().required(),
    userPassword: Joi.string().min(6).max(20),
    userRole: Joi.string().hex().required()
  },
  // DELETE /api/suppliers/staff/:staffId
  deleteSupplierStaff: {
    params: {
      staffId: Joi.string().hex().required()
    }
  },
  // PUT /api/suppliers/:supplierId/block
  // PUT /api/suppliers/:supplierId/unblock
  // GET /api/suppliers/:supplierId
  // DELETE /api/suppliers/:supplierId
  getDeleteOrBlockSupplier: {
    params: {
      supplierId: Joi.string().hex().required()
    }
  },
  // PUT /api/suppliers/:supplierId
  updateSupplier: {
    params: {
      supplierId: Joi.string().hex().required()
    },
    body: {
      representativeName: Joi.string().required(),
      commercialRegister: Joi.string().regex(/^[0-9]{10}$/).required(),
      commercialRegisterPhoto: Joi.string().required(),
      commercialRegisterExpireDate: Joi.date().required(),
      userFirstName: Joi.string().required(),
      userLastName: Joi.string().required(),
      userPassword: Joi.string().min(6).max(20).required(),
      userMobilePhone: Joi.string().regex(/^9665[0-9]{8}$/).required()
    }
  },
  // GET /api/suppliers/reports/orders
  // GET /api/suppliers/reports/transactions
  getReport: {
    query: {
      skip: Joi.number().integer().min(0).required(),
      limit: Joi.number().integer().min(1).required(),
      startDate: Joi.string().required(),
      endDate: Joi.string().required(),
      status: Joi.array()
    }
  },
  getTransactionReport: {
    query: {
      startDate: Joi.string().required(),
      endDate: Joi.string().required(),
      skip: Joi.number().integer().min(0).required(),
      limit: Joi.number().integer().min(1).required(),
      supplierId: Joi.string().hex(),
      customerId: Joi.string().hex(),
      type: Joi.string()
    }
  },

  // Transaction APIs
  // POST /api/transactions/declare
  declarePayment: {
    body: {
      amount: Joi.number().min(0).required(),
      supplierId: Joi.string().hex()
    }
  },
  // POST /api/transactions/add
  addPayment: {
    body: {
      amount: Joi.number().min(0).required(),
      supplierId: Joi.string().hex(),
      customerId: Joi.string().hex()
    }
  },

  // Payments APIs
  // PUT /api/payments/:paymentId/accept
  // PUT /api/payments/:paymentId/reject
  acceptOrRejectPayment: {
    params: {
      paymentId: Joi.string().hex().required()
    }
  },
  // GET /api/payments/pending
  getPayments: {
    query: {
      skip: Joi.number().integer().min(0).required(),
      limit: Joi.number().integer().min(1).required(),
      supplierId: Joi.string().hex(),
      customerId: Joi.string().hex(),
      isAdminFees: Joi.string(),
      status: Joi.array()
    }
  },

  // Forget Password APIs
  // POST /api/forgetPassword/forget
  forgetPassword: {
    body: {
      email: Joi.string().required()
    }
  },
  // PUT /api/forgetPassword/Reset
  resetPassword: {
    body: {
      password: Joi.string().min(6).max(20).required(),
      confirmPassword: Joi.string().min(6).max(20).required(),
      code: Joi.string().required()
    }
  },
  // GET /api/invoices
  getInvoice: {
    params: {
      invoiceId: Joi.string().hex().required()
    }
  },
};
