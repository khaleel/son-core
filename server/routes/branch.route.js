import express from 'express';
import passport from 'passport';
import branchCtrl from '../controllers/branch.controller';
import auth from '../services/Permissions/index';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(passport.authenticate('jwt', { session: false }), auth.can('Access Customers', 'GET_/api/branches'), branchCtrl.list)

  .post(passport.authenticate('jwt', { session: false }), auth.can('Access Customers', 'POST_/api/branches'), branchCtrl.create);

router.route('/:branchId')
  .put(passport.authenticate('jwt', { session: false }), auth.can('Access Customers', 'PUT_/api/branches'), branchCtrl.update)
  .get(passport.authenticate('jwt', { session: false }), auth.can('Access Customers', 'GET_/api/branches'), branchCtrl.get)
  .delete(passport.authenticate('jwt', { session: false }), auth.can('Access Customers', 'DELETE_/api/branches'), branchCtrl.remove);

router.param('branchId', branchCtrl.load);

export default router;
