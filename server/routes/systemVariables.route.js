import express from 'express';
// import passport from 'passport';
import systemVariablesCtrl from '../controllers/systemVariables.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(systemVariablesCtrl.list)

  .post(systemVariablesCtrl.create);

router.route('/:variableId')
  .put(systemVariablesCtrl.update)
  .get(systemVariablesCtrl.get)
  .delete(systemVariablesCtrl.remove);

router.param('variableId', systemVariablesCtrl.load);

export default router;
