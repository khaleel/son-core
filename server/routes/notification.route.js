import express from 'express';
import passport from 'passport';
import notificationCtrl from '../controllers/notification.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(passport.authenticate('jwt', { session: false }), notificationCtrl.list);

router.route('/:notificationId')
  .get(passport.authenticate('jwt', { session: false }), notificationCtrl.get)
  .put(passport.authenticate('jwt', { session: false }), notificationCtrl.update);

router.param('notificationId', notificationCtrl.load);

export default router;
