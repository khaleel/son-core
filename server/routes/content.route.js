import express from 'express';
// import passport from 'passport';
import contentCtrl from '../controllers/content.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(contentCtrl.list)

  .post(contentCtrl.create);

router.route('/:contentId')
  .put(contentCtrl.update)
  .get(contentCtrl.get)
  .delete(contentCtrl.remove);

router.param('contentId', contentCtrl.load);


export default router;
