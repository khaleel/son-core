import express from 'express';
// import passport from 'passport';
import contactCtrl from '../controllers/contact.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(contactCtrl.list)

  .post(contactCtrl.create);

router.route('/:contactId')
  .put(contactCtrl.update)
  .get(contactCtrl.get)
  .delete(contactCtrl.remove);

router.route('/reply/:contactId')
  .put(contactCtrl.reply);

router.param('contactId', contactCtrl.load);

export default router;
