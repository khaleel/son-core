import express from 'express';
// import passport from 'passport';
import emailCtrl from '../controllers/email.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(emailCtrl.list)
  .post(emailCtrl.create)
  .put(emailCtrl.update);

router.route('/:emailId')
  .get(emailCtrl.get);
router.route('/reset')
  .put(emailCtrl.reset);

router.param('emailId', emailCtrl.load);

export default router;
