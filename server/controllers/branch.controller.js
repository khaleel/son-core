import httpStatus from 'http-status';
import moment from 'moment-timezone';
import async from 'async';
import appSettings from '../../appSettings';
import Response from '../services/response.service';
import Branch from '../models/branch.model';
import Customer from '../models/customer.model';

/**
 * Get branch.
 * @returns {branch}
 */
function get(req, res) {
  const branch = req.branch;
  async.waterfall([
    function passParameter(callback) {
      callback(null, req.user._id);
    },
    getCustomerFromUser
  ], (err, result) => {
    if (err) {
      res.status(httpStatus.INTERNAL_SERVER_ERROR).json(Response.failure(err));
    } else if (branch.customer.toString() === result._id.toString()) {
      res.json(Response.success(branch));
    } else {
      res.status(httpStatus.UNAUTHORIZED).json(Response.failure(4));
    }
  });
}

/**
 * Get role list.
 * @returns {branch[]}
 */
function list(req, res) {
  async.waterfall([
    function passParameter(callback) {
      callback(null, req.user._id);
    },
    getCustomerFromUser
  ], (err, result) => {
    if (err) {
      res.status(httpStatus.INTERNAL_SERVER_ERROR).json(Response.failure(err));
    } else {
      Branch.find({ customer: result })
        .where('status').equals('Active')
        .sort({
          createdAt: -1
        })
        .then((branches) => {
          res.json(Response.success(branches));
        });
    }
  });
}

/**
 * Create order from recurring order
 * @returns {branch}
 */
function create(req, res) {
  async.waterfall([
    function passParameter(callback) {
      callback(null, req.user._id);
    },
    getCustomerFromUser
  ], (err, result) => {
    if (err) {
      res.status(httpStatus.INTERNAL_SERVER_ERROR).json(Response.failure(err));
    } else {
      const branch = new Branch({
        branchName: req.body.branchName,
        customer: result._id,
        location: req.body.location,
        createdAt: moment().tz(appSettings.timeZone).format(appSettings.momentFormat)
      });
      branch.save()
        .then(savedBranch => res.json(Response.success(savedBranch)))
        .catch(e => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(e));
    }
  });
}


/**
 * Update a branch
 */
function update(req, res) {
  const branch = req.branch;
  async.waterfall([
    function passParameter(callback) {
      callback(null, req.user._id);
    },
    getCustomerFromUser
  ], (err, result) => {
    if (err) {
      res.status(httpStatus.INTERNAL_SERVER_ERROR).json(Response.failure(err));
    } else if (branch.customer.toString() === result._id.toString()) {
      branch.branchName = req.body.branchName;
      branch.location = req.body.location;
      branch.save()
        .then(savedBranch => res.json(Response.success(savedBranch)))
        .catch(e => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(Response.failure(e)));
    } else {
      res.status(httpStatus.UNAUTHORIZED).json(Response.failure(4));
    }
  });
}

/**
 * Delete branch.
 * @returns {branch}
 */
function remove(req, res) {
  const branch = req.branch;
  async.waterfall([
    function passParameter(callback) {
      callback(null, req.user._id);
    },
    getCustomerFromUser
  ], (err, result) => {
    if (err) {
      res.status(httpStatus.INTERNAL_SERVER_ERROR).json(Response.failure(err));
    } else if (branch.customer.toString() === result._id.toString()) {
      branch.status = 'InActive';
      branch.save()
        .then(savedBranch => res.json(Response.success(savedBranch)))
        .catch(e => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(Response.failure(e)));
    } else {
      res.status(httpStatus.UNAUTHORIZED).json(Response.failure(4));
    }
  });
}

/**
 * Load.
 * @returns {branch}
 */
function load(req, res, next, id) {
  Branch.findById(id)
    .then((branch) => {
      if (branch) {
        req.branch = branch;
        return next();
      }
      return res.status(httpStatus.BAD_REQUEST).json(Response.failure(2));
    })
    .catch(e => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(Response.failure(e)));
}

/**
 * Helper Fucntions
 */
function getCustomerFromUser(user, callback) {
  Customer.findOne({ $or: [{ user }, { staff: { $in: [user] } }] })
    .then((customer) => {
      callback(null, customer);
    });
}

export default {
  load,
  get,
  list,
  create,
  update,
  remove,
};

